# Containeraization of af Frontend, backend and DB. 
 This is a small project about containers, Vue 3, Strapi3 og MySQL DB. 

 ## How to start the project

   1. Clone the project
   2. Go to project root 
   3. Open your CMD
   4. Write 


   ```
   Docker compose up -d
```
   
   4. Take a small coffee break while everything builds and strapi starts. 
   5. View the frontend at: http://localhost:8012/ 
   6. View strapi at: http://localhost:1337/admin 
   7. Login a strapi with: <br>
   Username: katharinaappel@gmail.com <br>
   Password: Henning1337

   To terminate the containers run: 

  ```
   Docker compose down
```


